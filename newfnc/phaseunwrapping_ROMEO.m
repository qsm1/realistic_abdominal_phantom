function [phi] = phaseunwrapping_ROMEO(S, TE, mask)

TE = TE/1000;
phi = angle(S);
mag = squeeze(abs(S));

%romeo struct
params.output_dir                 = fullfile(tempdir, 'romeo_tmp');
params.TE                         = TE;
params.mag                        = mag;
params.mask                       = mask;
params.calculate_B0               = false;
params.phase_offset_correction    = 'false';
params.voxel_size                 = [2,2,2];
params.additional_flags           = '--verbose -q';

%romeo
mkdir(params.output_dir);
[phi, ~] = ROMEO(phi, params);
rmdir(params.output_dir, 's')
end