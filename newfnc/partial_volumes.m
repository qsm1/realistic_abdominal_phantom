function [chi_synth] = partial_volumes(chi_synth, labels, sigma, exclude_list)

Nx = size(labels);
Nt = max(labels(:));
Pt = zeros([Nx,Nt]);

%Filter excluded tissues
t_exclude  = zeros(Nt,1);
t_exclude(exclude_list) = 1;

for t = 1:Nt
    Mt = double(labels == t);
    if t_exclude(t)
        Pt(:,:,:,t) = Mt;
    else
%         pt_aux = imgaussfilt3(Mt,sigma);
%         pt_aux(Mt == 1) = 1;
%         Pt(:,:,:,t) = pt_aux;
        Pt(:,:,:,t) = imgaussfilt3(Mt,sigma);
    end
end

%Normalize probabilities
Pt = Pt ./ sum(Pt,4);
Pt(isnan(Pt)) = 0;

%Apply partial volumes
chi_synth = sum(Pt .* chi_synth,4);

end

