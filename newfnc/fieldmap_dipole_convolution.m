function [phi] = fieldmap_dipole_convolution(chi_synth,voxel_size,type)

Msize = size(chi_synth);
D     = dipole_kernel_fansi(Msize, voxel_size, type);
phi   = ifftn(D .* fftn(chi_synth));

%Crop
N1 = Msize/3 + 1;
N2 = Msize/3 * 2;

%Local
phi   = phi(N1(1):N2(1), N1(2):N2(2), N1(3):N2(3));
end

