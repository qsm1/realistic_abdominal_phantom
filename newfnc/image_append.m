function [out] = image_append(X,n)

out = [];
for i = 1:n
    out = [out, X(:,:,:,i)];
end
end
