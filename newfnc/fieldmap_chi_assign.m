function [chi_synth, mask] = fieldmap_chi_assign(model,labels,wtr_norm,fat_norm,r2s_norm)

%Relevant sizes
Nx = size(labels);      %phantom size
Nt = max(labels(:));    %number of labels

%Assign susceptibilities
chi_synth = zeros([Nx,Nt]);
for t = 1:Nt    
    msg = fprintf("Assigning chi values: tissue %2i of %2i \n",t,Nt);
    %Auxiliar mask
%     Mt = double(labels == t); 
    %Susceptibility model
% %     chi_mean = Mt .* model(t, 1);
% %     r2s_mod = model(t, 5) .* Mt .* (r2s_norm - model(t, 2));
% %     wtr_mod = model(t, 6) .* Mt .* (wtr_norm - model(t, 3));
% %     fat_mod = model(t, 7) .* Mt .* (fat_norm - model(t, 4));
% %     chi_mod = chi_mod + chi_mean + r2s_mod + wtr_mod + fat_mod;
    chi_mean = model(t, 1);
    r2s_mod = model(t, 5) .* (r2s_norm - model(t, 2));
    wtr_mod = model(t, 6) .* (wtr_norm - model(t, 3));
    fat_mod = model(t, 7) .* (fat_norm - model(t, 4));
    chi_synth(:,:,:,t) = chi_mean + r2s_mod + wtr_mod + fat_mod;
    fprintf(repmat('\b',1,msg));
end
fprintf("Assigning chi values: tissue %2i of %2i \n",t,Nt);

%Generate mask
mask = double(labels > 0);
end

