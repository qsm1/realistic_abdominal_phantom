function [R,F,P,R2, outParams_GC] = runIGC(imDataParams, fatmodel, lambda, iters)

%% Set recon parameters
% General parameters
algoParams.species(1).name          = 'water';
algoParams.species(1).frequency     = 0;
algoParams.species(1).relAmps       = 1;
algoParams.species(2).name          = 'fat';
algoParams.species(2).frequency     = fatmodel.fat_freqs;
algoParams.species(2).relAmps       = fatmodel.fat_ramps;

% Algorithm-specific parameters
algoParams.size_clique              = 1;            % Size of MRF neighborhood (1 uses an 8-neighborhood, common in 2D)
algoParams.range_r2star             = [0 300];      % Range of R2* values
algoParams.NUM_R2STARS              = 11;           % Numbre of R2* values for quantization
algoParams.range_fm                 = [-400,400];   % Range of field map values
algoParams.NUM_FMS                  = 301;          % Number of field map values to discretize
algoParams.NUM_ITERS                = iters;
algoParams.SUBSAMPLE                = 2;            % Spatial subsampling for field map estimation (for speed)
algoParams.DO_OT                    = 1;            % 0,1 flag to enable optimization transfer descent (final stage of field map estimation)
algoParams.LMAP_POWER               = 2;            % Spatially-varying regularization (2 gives ~ uniformn resolution)
algoParams.lambda                   = lambda;
algoParams.LMAP_EXTRA               = 0.05;         % More smoothing for low-signal regions
algoParams.TRY_PERIODIC_RESIDUAL    = 0;
algoParams.THRESHOLD                = 0.04;
algoParams.NUM_MAGN                 = 1;

%%      %Excecute Algorithm
imDataParams_2          = imDataParams;
outParams_GC            = cell([size(imDataParams.images, 3) 1]);
DO_MIXED_FIT            = 0;

tic
for i=1:size(imDataParams.images, 3)
    igc_msg = fprintf('Hernando IGC: Slice %3i of %3i\n',i,size(imDataParams.images, 3));
    imDataParams_2.images = imDataParams.images(:, :, i, :, :);
    if i == 1
        outParams_GC = fw_i2cm1i_3pluspoint_hernando_graphcut( imDataParams_2, algoParams );
   
        if DO_MIXED_FIT > 0 
            try
                algoParams.fieldmap = outParams_GC.fieldmap(:,:,i);
                algoParams.r2starmap = outParams_GC.r2starmap(:,:,i);
                outParams_GC_M = fw_i2xm1c_3pluspoint_hernando_mixedfit( imDataParams_2, algoParams );
            end
        end
    else
        outParams_GC_2 = fw_i2cm1i_3pluspoint_hernando_graphcut( imDataParams_2, algoParams );
        outParams_GC.species(1).amps = cat(3, outParams_GC.species(1).amps, outParams_GC_2.species(1).amps);
        outParams_GC.species(2).amps = cat(3, outParams_GC.species(2).amps, outParams_GC_2.species(2).amps);
        outParams_GC.fieldmap = cat(3, outParams_GC.fieldmap, outParams_GC_2.fieldmap);
        outParams_GC.r2starmap = cat(3, outParams_GC.r2starmap, outParams_GC_2.r2starmap);
        
        if DO_MIXED_FIT > 0 
            try
                algoParams.fieldmap = outParams_GC.fieldmap(:,:,i);
                algoParams.r2starmap = outParams_GC.r2starmap(:,:,i);
                outParamsMixed = fw_i2xm1c_3pluspoint_hernando_mixedfit( imDataParams_2, algoParams );
                outParams_GC_M.species(1).amps = cat(3, outParams_GC_M.species(1).amps, outParamsMixed.species(1).amps);
                outParams_GC_M.species(2).amps = cat(3, outParams_GC_M.species(2).amps, outParamsMixed.species(2).amps);
                outParams_GC_M.fieldmap = cat(3, outParams_GC_M.fieldmap, outParamsMixed.fieldmap);
                outParams_GC_M.r2starmap = cat(3, outParams_GC_M.fieldmap, outParamsMixed.r2starmap);
            end
        end
    end
    fprintf(repmat('\b',1,igc_msg));
end
fprintf('Hernando IGC: Slice %3i of %3i\n',i,size(imDataParams.images, 3));
toc

%%      %Outputs
fat         = outParams_GC.species(2).amps;
water       = outParams_GC.species(1).amps;
fieldmap	= outParams_GC.fieldmap;
r2star      = outParams_GC.r2starmap;

R           = cat(4, water, fat);
P           = fieldmap;
R2          = r2star;
F           = abs(fat)./(abs(fat)+abs(water))*100;
end