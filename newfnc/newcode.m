% Load Variables
fpath = 'tissue_properties/';
load([fpath, 'labels_v2']);
load([fpath, 'r2s_v2']);
load([fpath, 'ph0_v2']);
load([fpath, 'wtr_v2']);
load([fpath, 'fat_v2']);
load([fpath, 'r2s_norm_v2']);
load([fpath, 'wtr_norm_v2']);
load([fpath, 'fat_norm_v2']);

%ph0 = zeros(size(labels));

%% Options for Phantom Generation

% Load Models
fpath = 'models/paper/HS/';
chi_model = csvread([fpath,'chi_model.csv'],1,1);
r2s_model = csvread([fpath,'r2s_model.csv'],1,1);

%% Main code

%Local susceptibility
[chi_L, mask] = fieldmap_chi_assign(chi_model,labels,wtr_norm,fat_norm,r2s_norm);
[chi_L, mask] = fieldmap_exclude_tissues(chi_L,mask,labels,[12,21,23]);
%[chi_L,labels_new,~] = fieldmap_include_lobe(chi_L, labels);
[chi_L] = partial_volumes(chi_L, labels, 0.5, [12,21,23]);
[chi_gt] = fieldmap_demean(chi_L, mask);

%Background susceptibility
chi_BG = fieldmap_background(chi_model,labels,[12,23],9.2);

%Dipole convolution
chi_pad = fieldmap_padding(chi_L, 0);
phiL = fieldmap_dipole_convolution(chi_pad,[2,2,2],0);
chi_pad = fieldmap_padding(chi_L + chi_BG, 1);
phi = fieldmap_dipole_convolution(chi_pad,[2,2,2],0);


%R2s map
[r2s_synth,r2s_mask] = r2star_r2s_assign(r2s_model,labels,r2s_norm,r2s);
%[r2s_synth, r2s_mask_new] = r2star_include_lobe(r2s_synth, r2s_mask);
[r2s_synth] = partial_volumes(r2s_synth, r2s_mask, 0.5, []);

%% Signal model
% Select echo times
TE = linspace(1.8, 9.6, 6); % Experimental CSE acquisition
%TE = linspace(2.3, 11.5, 5); % 1peak-based in-phase acquisition
%TE = [2.34, 4.56, 6.79, 9.16, 11.60]; %6peak-based in-phase acquisition
fatmodel = FatPeaks(6);
[pdff, S, Suf, Sw, Sf] = signalmodel_simulate(phi, r2s_synth, mask, wtr, fat, ph0, TE, fatmodel);

%% Romeo unwrapping 
