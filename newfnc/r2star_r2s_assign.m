function [r2s_synth, r2s_mask] = r2star_r2s_assign(model,labels,r2s_norm,r2s)

%Relevant sizes
Nx = size(labels);
Nt = max(labels(:)); %number of labels

%Assign R2* values
r2s_synth = r2s;
r2s_mask  = double(labels > 0);
count = 1;

for t = 1:Nt
    msg = fprintf("Assigning R2s values: tissue %2i of %2i \n",t,Nt);
    %Tissues selected to be modified
    if (model(t,1) == 1)
        %R2s mask
        count = count +1;
        Mt = double(labels) == t;
        r2s_mask = r2s_mask .* (1-Mt) + Mt * count;
        
        %R2s value
        r2s_mean  = model(t, 2);
        r2s_mod   = model(t, 4) .* (r2s_norm - model(t, 3));
        r2s_mod   = r2s_mean + r2s_mod;
        r2s_synth(:,:,:,end+1) = r2s_mod;
    end
    fprintf(repmat('\b',1,msg));
end
fprintf("Assigning R2s values: tissue %2i of %2i \n",t,Nt);
end

