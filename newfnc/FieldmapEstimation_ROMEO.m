function [phi, r2s, Nstd] = FieldmapEstimation_ROMEO(S, TE, mask)

Nx = size(S);
TE = TE/1000;

% Complex Fitting
[phi,Nstd] = Fit_ppm_complex(S .* mask);
% r2s map
[~,r2s] = t2s(S, TE, ones(Nx));

%romeo struct
params.output_dir                 = fullfile(tempdir, 'romeo_tmp');
params.TE                         = TE;
params.mag                        = abs(squeeze(S));
params.mask                       = mask;
params.calculate_B0               = false;
params.phase_offset_correction    = 'false';
params.voxel_size                 = [2,2,2];
params.additional_flags           = '--verbose -q';

%romeo
mkdir(params.output_dir);
[phi, ~] = ROMEO(phi, params);
phi = -phi .* mask;
%phi = phi / (3 * 42.58 * 2 * pi * TE(end));
rmdir(params.output_dir, 's')
end