function [chi_synth] = fieldmap_background(model,labels,include_list,chi_ext_air)

Nx = size(labels);
Nt = max(labels(:));

t_include  = zeros(Nt,1);
t_include(include_list) = 1;

chi_synth = zeros([Nx,Nt]);
for t = 1:Nt
    if t_include(t)
        Mt = double(labels == t);
        chi_synth(:,:,:,t) = Mt .* model(t,1);
    end
end

chi_synth(:,:,:,end+1) = double(labels == 0) * chi_ext_air;
chi_synth = sum(chi_synth,4);
end

