function [pdff, S, Suf, Sw, Sf] = signalmodel_simulate(phi, r2s, mask, wtr, fat, ph0, TE, fatmodel)


% Variables
wtr = wtr .* mask;
fat = fat .* mask;
r2s = r2s .* mask;
phi = phi .* mask;
ph0 = ph0 .* mask;
TE  = TE / 1000;
df  = fatmodel.fat_freqs;
da  = fatmodel.fat_ramps; 
N   = size(phi);

% Constant parameters
FS = 3;                 %Field strength
gyro = 2 * pi * 42.58;  % gyromagnetic ratio

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Generate PDFF-related variables %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% pdff
pdff = abs(fat) ./ (abs(wtr) + abs(fat));
pdff(isnan(pdff)) = 0;

% phi0
ph0_w = ph0 .* (1 - pdff);
ph0_f = ph0 .* (pdff);

%%%%%%%%%%%%%%%%%%%%
%%% Signal model %%%
%%%%%%%%%%%%%%%%%%%%
% Fat contributions 
Sf_mod = sum(da .* exp(1i * FS * gyro *(TE' * df)),2);

%Model
S   = zeros([N, length(TE)]);
Sw  = zeros([N, length(TE)]);
Sf  = zeros([N, length(TE)]);
Suf = zeros([N, length(TE)]);  
    
for echo = 1:length(TE)
    msg = fprintf("Forward Simulation: echo %2i of %2i \n",echo,length(TE));
    chi_phase = phi * 3 * gyro * TE(echo);  
    r2s_decay = - TE(echo) * r2s;
    
    Suf(:,:,:,echo) = (wtr + fat) .* exp(r2s_decay + 1i * (chi_phase + ph0));
    Sw(:,:,:,echo) = wtr .* exp(r2s_decay + 1i * (chi_phase + ph0_w));
    Sf(:,:,:,echo) = fat * Sf_mod(echo) .* exp(r2s_decay + 1i * (chi_phase + ph0_f));

    
    S(:,:,:,echo)  = Sw(:,:,:,echo) + Sf(:,:,:,echo);
    fprintf(repmat('\b',1,msg));
end
fprintf("Forward Simulation: echo %2i of %2i \n",echo,length(TE));

%Normalize to 0-1 range
%Sw  = Sw  ./ max(abs(Sw(:)));
%Sf  = Sf  ./ max(abs(Sf(:)));
S   = S   ./ max(abs(S(:)));
Suf = Suf ./ max(abs(Suf(:)));
end
    