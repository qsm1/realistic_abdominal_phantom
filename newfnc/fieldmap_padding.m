function [chi_pad] = fieldmap_padding(chi_synth,type)
    Msize = size(chi_synth);
    r1 = (Msize(1) + 1):(2*Msize(1));
    r2 = (Msize(2) + 1):(2*Msize(2));
    r3 = 1:Msize(3);
    r4 = (2 * Msize(3) + 1): (3*Msize(3));
    
    chi_pad  = padarray(chi_synth, Msize, chi_synth(1,1,1), 'both');
    
    if type == 1    
        chi_pad(r1,r2,r3) = chi_synth(:,:,end:-1:1);
        chi_pad(r1,r2,r4) = chi_synth(:,:,end:-1:1);
    end
end

