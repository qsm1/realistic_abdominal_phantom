function [chi_synth] = fieldmap_blur_old(chi_synth,labels,t_exclude)

N = size(chi_synth);
Nx = N(1:3);
Nt = N(4);

mask = zeros(size(chi_synth));
for t = 1:Nt
    mask(:,:,:,t) = double(labels) == t;
end
    

chi_synth = sum(chi_synth.*mask,4);

%t_exclude = [12,13,14,21,23];
t_valid   = ones(Nt,1);
t_valid(t_exclude) = 0;

Pt = zeros([Nx,Nt]);
sigma = 0.51;
for t = 1:Nt
    %auxiliar mask
    Mt = double(labels == t);
    %generate blurred mask
    if t_valid(t)
        Pt(:,:,:,t) = imgaussfilt3(Mt,sigma);
    else
        Pt(:,:,:,t) = Mt;
    end
end

%Normalize probabilities
Pt = Pt ./ sum(Pt,4);
Pt(isnan(Pt)) = 0;

%Apply partial volumes
chi_pv = zeros(Nx);
for t = 1:Nt
    msg = fprintf("Partial Volume Effects: tissue %2i of %2i \n",t,Nt);
    
    %Auxiliar mask
    Mt = double(labels == t);
    chi_pv = chi_pv + Pt(:,:,:,t) .* Mt .* chi_synth;
    
    fprintf(repmat('\b',1,msg));
end
fprintf("Partial Volume Effects: tissue %2i of %2i \n",t,Nt);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Adittional Smoothing %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%parameters
sigma = 0.6;
thres = 0.25;

%edge mask
Me = double(imgradient3(labels));
Me = Me ./ max(Me(:));
Me = Me > thres;

%blur edges and apply to phantom
Be = imgaussfilt3(chi_pv, sigma) .* Me;
chi_synth = Be + chi_pv .* (1 - Me);


end

