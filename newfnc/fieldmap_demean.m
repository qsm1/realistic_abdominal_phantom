function [X_demean] = fieldmap_demean(X,mask)

X_demean = X - mean(X(mask == 1));

end

