function [chi_synth, mask] = fieldmap_exclude_tissues(chi_synth,mask,labels,exclude_list)

%Filter excluded tissues
Nt = max(labels(:));
t_exclude  = zeros(Nt,1);
t_exclude(exclude_list) = 1;

for t = 1:Nt    
    msg = fprintf("Checking for excluded tissues: tissue %2i of %2i \n",t,Nt);
    if t_exclude(t)
        %Remove tissue
        Mt = 1 - double(labels==t);
%         chi_mod = chi_mod .* Mt;
        mask = mask .* Mt;
        chi_synth(:,:,:,t) = 0;
    end
    fprintf(repmat('\b',1,msg));
end
fprintf("Checking for excluded tissues: tissue %2i of %2i \n",t,Nt);
end

