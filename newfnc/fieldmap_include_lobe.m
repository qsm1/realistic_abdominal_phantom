function [chi_synth, labels_new] = fieldmap_include_lobe(lobe, lobe_mean, lobe_alpha, chi_synth, labels)

%Load lobe
lobe_mask = lobe.lobe_mask;
lobe_text = lobe.lobe_texture;

%Redefine labels
labels_new = double(labels) .* (1-lobe_mask) + lobe_mask * 24;

%Redefine_chi
lobe_chi = (lobe_mean + lobe_alpha .* lobe_text) .* lobe_mask;
lobe_chi = lobe_chi + (1-lobe_mask) .* lobe_mean;
chi_synth(:,:,:,end+1) = lobe_chi;
end

