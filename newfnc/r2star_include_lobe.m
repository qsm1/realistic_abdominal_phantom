function [r2s_synth, r2s_mask_new] = r2star_include_lobe(lobe,lobe_mean, lobe_alpha, r2s_synth, r2s_mask)

%Load lobe
lobe_mask = lobe.lobe_mask;
lobe_text = lobe.lobe_texture;

%Redefine r2s_mask
r2s_mask_new = double(r2s_mask) .* (1-lobe_mask) + lobe_mask * (max(r2s_mask(:))+1);

%Redefine_chi
lobe_r2s = (lobe_mean + lobe_alpha .* lobe_text) .* lobe_mask;
lobe_r2s = lobe_r2s + (1-lobe_mask) .* lobe_mean;
r2s_synth(:,:,:,end+1) = lobe_r2s;
end