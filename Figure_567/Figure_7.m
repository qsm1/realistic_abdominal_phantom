P1 = load('IOp1_QSM.mat');
P2 = load('IOp2_QSM.mat');


%% Crops
ax1 = 46:150; ax2 = 21:175; ax3 = 96;
sg1 = 46:150; sg2 = 50:155; sg3 = 96;
cr1 = 46:150; cr2 = 21:175; cr3 = 96;

%% Spacing
vspace = 100 * ones([30, 416]); 
hspace = 100 * ones([525, 5]);
cspace = 100 * ones([30, 5]);
%% Ground truth
ax_gt = padarray(P1.chi_gt.*P1.mask,[0,0,56],0,'both');
sg_gt = flip(permute(ax_gt,[3,1,2]),1);
cr_gt = flip(permute(ax_gt,[3,2,1]),1);

x_gt = [ax_gt(ax1,ax2,ax3),cr_gt(cr1,cr2,cr3),sg_gt(sg1,sg2,sg3);...
        ax_gt(ax1,ax2,ax3),cr_gt(cr1,cr2,cr3),sg_gt(sg1,sg2,sg3);...
        ax_gt(ax1,ax2,ax3),cr_gt(cr1,cr2,cr3),sg_gt(sg1,sg2,sg3);...
        ax_gt(ax1,ax2,ax3),cr_gt(cr1,cr2,cr3),sg_gt(sg1,sg2,sg3);...
        ax_gt(ax1,ax2,ax3),cr_gt(cr1,cr2,cr3),sg_gt(sg1,sg2,sg3)];

%% Protocol 1

p1ax_p1 = padarray(P1.STAR_chi,[0,0,56],0,'both');
p1ax_p2 = padarray(P1.NDI_chi,[0,0,56],0,'both');
p1ax_p3 = padarray(P1.MEDI_chi,[0,0,56],0,'both');
p1ax_p4 = padarray(P1.FANSI_chi,[0,0,56],0,'both');
p1ax_p5 = padarray(P1.HD_chi,[0,0,56],0,'both');

p1sg_p1 = flip(permute(p1ax_p1,[3,1,2]),1);
p1sg_p2 = flip(permute(p1ax_p2,[3,1,2]),1);
p1sg_p3 = flip(permute(p1ax_p3,[3,1,2]),1);
p1sg_p4 = flip(permute(p1ax_p4,[3,1,2]),1);
p1sg_p5 = flip(permute(p1ax_p5,[3,1,2]),1);

p1cr_p1 = flip(permute(p1ax_p1,[3,2,1]),1);
p1cr_p2 = flip(permute(p1ax_p2,[3,2,1]),1);
p1cr_p3 = flip(permute(p1ax_p3,[3,2,1]),1);
p1cr_p4 = flip(permute(p1ax_p4,[3,2,1]),1);
p1cr_p5 = flip(permute(p1ax_p5,[3,2,1]),1);

x_p1 = [p1ax_p1(ax1,ax2,ax3),p1cr_p1(cr1,cr2,cr3),p1sg_p1(sg1,sg2,sg3);...
        p1ax_p2(ax1,ax2,ax3),p1cr_p2(cr1,cr2,cr3),p1sg_p2(sg1,sg2,sg3);...
        p1ax_p3(ax1,ax2,ax3),p1cr_p3(cr1,cr2,cr3),p1sg_p3(sg1,sg2,sg3);...
        p1ax_p4(ax1,ax2,ax3),p1cr_p4(cr1,cr2,cr3),p1sg_p4(sg1,sg2,sg3);...
        p1ax_p5(ax1,ax2,ax3),p1cr_p5(cr1,cr2,cr3),p1sg_p5(sg1,sg2,sg3)];
p1_diff = x_p1 - x_gt;

%% Protocol 2

p2ax_p1 = padarray(P2.STAR_chi,[0,0,56],0,'both');
p2ax_p2 = padarray(P2.NDI_chi,[0,0,56],0,'both');
p2ax_p3 = padarray(P2.MEDI_chi,[0,0,56],0,'both');
p2ax_p4 = padarray(P2.FANSI_chi,[0,0,56],0,'both');
p2ax_p5 = padarray(P2.HD_chi,[0,0,56],0,'both');

p2sg_p1 = flip(permute(p2ax_p1,[3,1,2]),1);
p2sg_p2 = flip(permute(p2ax_p2,[3,1,2]),1);
p2sg_p3 = flip(permute(p2ax_p3,[3,1,2]),1);
p2sg_p4 = flip(permute(p2ax_p4,[3,1,2]),1);
p2sg_p5 = flip(permute(p2ax_p5,[3,1,2]),1);

p2cr_p1 = flip(permute(p2ax_p1,[3,2,1]),1);
p2cr_p2 = flip(permute(p2ax_p2,[3,2,1]),1);
p2cr_p3 = flip(permute(p2ax_p3,[3,2,1]),1);
p2cr_p4 = flip(permute(p2ax_p4,[3,2,1]),1);
p2cr_p5 = flip(permute(p2ax_p5,[3,2,1]),1);

x_p2 = [p2ax_p1(ax1,ax2,ax3),p2cr_p1(cr1,cr2,cr3),p2sg_p1(sg1,sg2,sg3);...
        p2ax_p2(ax1,ax2,ax3),p2cr_p2(cr1,cr2,cr3),p2sg_p2(sg1,sg2,sg3);...
        p2ax_p3(ax1,ax2,ax3),p2cr_p3(cr1,cr2,cr3),p2sg_p3(sg1,sg2,sg3);...
        p2ax_p4(ax1,ax2,ax3),p2cr_p4(cr1,cr2,cr3),p2sg_p4(sg1,sg2,sg3);...
        p2ax_p5(ax1,ax2,ax3),p2cr_p5(cr1,cr2,cr3),p2sg_p5(sg1,sg2,sg3)];    
p2_diff = x_p2 - x_gt;



%% Display
figure
imshow(1*[x_p1, hspace 2 * p1_diff; ...
        vspace, cspace, vspace;...
        x_p2, hspace, 2 * p2_diff],[-1,1]);
set(gca,'Position',[0 0 1 1]);
print('Figure_7','-dpng','-r300')