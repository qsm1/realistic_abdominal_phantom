%% Load phantoms
gt = load('Fig4_GT.mat');
p1 = load('Fig4_HS.mat');
p2 = load('Fig4_PL.mat');
p3 = load('Fig4_IO.mat');

%% Figure 4a: Fieldmap differences
hsp = 1000*ones([105,10]);
%Shape
ax1 = 46:150; ax2 = 21:175; ax3 = 40;
mask = gt.mask(ax1,ax2,ax3);
%Fieldmaps
phi_gt_HS = gt.phiL_gt_HS(ax1,ax2,ax3).* mask;
phi_gt_PL = gt.phiL_gt_PL(ax1,ax2,ax3).* mask;
phi_gt_IO = gt.phiL_gt_IO(ax1,ax2,ax3).* mask;

phi_gc_HS = p1.phiL_gc(ax1,ax2,ax3) .* mask;
phi_gc_PL = p2.phiL_gc(ax1,ax2,ax3) .* mask;
phi_gc_IO = p3.phiL_gc(ax1,ax2,ax3) .* mask;

phi_ip_HS = p1.phiL_ip(ax1,ax2,ax3) .* mask;
phi_ip_PL = p2.phiL_ip(ax1,ax2,ax3) .* mask;
phi_ip_IO = p3.phiL_ip(ax1,ax2,ax3) .* mask;

phi_HS = [phi_gc_HS,phi_ip_HS,hsp;... 
          4*(phi_gc_HS - phi_gt_HS), 4*(phi_ip_HS - phi_gt_HS),hsp];
phi_PL = [phi_gc_PL,phi_ip_PL,hsp;... 
          4*(phi_gc_PL - phi_gt_PL), 4*(phi_ip_PL - phi_gt_PL),hsp]; 
phi_IO = [phi_gc_IO,phi_ip_IO;... 
          4*(phi_gc_IO - phi_gt_IO), 4*(phi_ip_IO - phi_gt_IO)]; 

figure; imshow([phi_HS,phi_PL,phi_IO],[-0.5 0.5]);
set(gca,'Position',[0 0 1 1]);
print('Figure_4a','-dpng','-r300')

%% Graph Cuts Fails;
mask1 = gt.mask(ax1,ax2,30);
mask2 = gt.mask(ax1,ax2,40);

phiT_gc_HS = [(p1.phiT_gc(ax1,ax2,30) + 1*pi) .* mask1; (p1.phiT_gc(ax1,ax2,40) + 1*pi) .* mask2];
phiT_gc_PL = [(p2.phiT_gc(ax1,ax2,30) + 1*pi) .* mask1; (p2.phiT_gc(ax1,ax2,40) + 1*pi) .* mask2];
phiT_gc_IO = [(p3.phiT_gc(ax1,ax2,30) + 1*pi) .* mask1; (p3.phiT_gc(ax1,ax2,40) + 1*pi) .* mask2];

phiT_ip_HS = [(p1.phiT_ip(ax1,ax2,30) - 0*pi) .* mask1; (p1.phiT_ip(ax1,ax2,40) + 0*pi) .* mask2];
phiT_ip_PL = [(p2.phiT_ip(ax1,ax2,30) - 0*pi) .* mask1; (p2.phiT_ip(ax1,ax2,40) + 0*pi) .* mask2];
phiT_ip_IO = [(p3.phiT_ip(ax1,ax2,30) - 0*pi) .* mask1; (p3.phiT_ip(ax1,ax2,40) + 0*pi) .* mask2];

hsp = 1000*ones([210,10]);
HS = [phiT_gc_HS,phiT_ip_HS];
PL = [phiT_gc_PL,phiT_ip_PL];
IO = [phiT_gc_IO,phiT_ip_IO];
imshow([HS,hsp,PL,hsp,IO],[-2,2])

set(gca,'Position',[0 0 1 1]);
print('Figure_4b','-dpng','-r300')

%% Discrepancy
ax3 = 40;
mask = repmat(gt.mask(ax1,ax2,ax3),[1,1,1,5]);
hsp = 1000*ones([105,10]);
phiL_HS_uf = image_append(p1.phiL_ip_uw(ax1,ax2,ax3,:) .* mask, 5);
phiL_PL_uf = image_append(p2.phiL_ip_uw(ax1,ax2,ax3,:) .* mask, 5);
phiL_IO_uf = image_append(p3.phiL_ip_uw(ax1,ax2,ax3,:) .* mask, 5);

phiL_HS_ip = image_append(p1.SufLp2_ip(ax1,ax2,ax3,:) .* mask, 5);
phiL_PL_ip = image_append(p2.SufLp2_ip(ax1,ax2,ax3,:) .* mask, 5);
phiL_IO_ip = image_append(p3.SufLp2_ip(ax1,ax2,ax3,:) .* mask, 5);

disc_HS = 4*(phiL_HS_ip - phiL_HS_uf);
disc_PL = 4*(phiL_PL_ip - phiL_PL_uf);
disc_IO = 4*(phiL_IO_ip - phiL_IO_uf);

figure;
imshow([phiL_HS_ip; disc_HS;...
          %phiL_PL_ip; disc_PL;...
          %phiL_IO_ip; disc_IO
          ],[-5,5])
      
set(gca,'Position',[0 0 1 1]);
print('Figure_4c','-dpng','-r300')