%% Variables and model

% Load Variables
fpath = 'tissue_properties/';
load([fpath, 'labels']);
load([fpath, 'r2s']);
load([fpath, 'ph0']);
load([fpath, 'wtr']);
load([fpath, 'fat']);
load([fpath, 'r2s_norm']);
load([fpath, 'wtr_norm']);
load([fpath, 'fat_norm']);

% Load Model
fpath = 'models/paper/IO/';
chi_model = csvread([fpath,'chi_model.csv'],1,1);
r2s_model = csvread([fpath,'r2s_model.csv'],1,1);
ph0 = zeros(size(fat_norm));

% Parameters
voxel_size = [1.9792, 1.9792, 2.0000];
fatmodel.fat_ramps  = [ 0.087,  0.693,  0.128,  0.004,  0.039, 0.048];
fatmodel.fat_freqs  = [-3.800, -3.400, -2.600, -1.940, -0.390, 0.600];

% Fieldmap

%Local susceptibility
[chi_L, mask] = fieldmap_chi_assign(chi_model,labels,wtr_norm,fat_norm,r2s_norm);
[chi_L, mask] = fieldmap_exclude_tissues(chi_L,mask,labels,[12,21,23]);
[chi_L] = partial_volumes(chi_L, labels, 0.5, [12,21,23]);
[chi_gt] = fieldmap_demean(chi_L, mask);

%Background susceptibility
chi_BG = fieldmap_background(chi_model,labels,[12,23],9.2);

%Dipole convolution
chi_pad = fieldmap_padding(chi_L, 0);
phiL = fieldmap_dipole_convolution(chi_pad,voxel_size,0);
chi_pad = fieldmap_padding(chi_L + chi_BG, 1);
phi = fieldmap_dipole_convolution(chi_pad,voxel_size,0);

% Signal model

%R2s map
[r2s_synth,r2s_mask] = r2star_r2s_assign(r2s_model,labels,r2s_norm,r2s);
[r2s_synth] = partial_volumes(r2s_synth, r2s_mask, 0.5, []);


%save
save('Figure_2/Fig2_IO.mat','chi_gt','mask','phiL','phi','r2s_synth')