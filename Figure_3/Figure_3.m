%Load phantoms and variables
ex = load('Fig3_Exp.mat');
p1 = load('Fig3_HS.mat');
p2 = load('Fig3_PL.mat');
p3 = load('Fig3_IO.mat');


%% Experimental data
%Spacing
hsp = 10 * ones([105,10]);
vsp = 10 * ones([5,950]);

%Magnitude and phase
Mexp = abs(ex.SExp)./ max(abs(ex.SExp(:)));
Pexp = ex.SExp_uw;

%Shape
ax1 = 46:150; ax2 = 21:175; ax3 = 40;
mask = ex.mask;

%Offset correction
Pexp(:,:,:,1) = (Pexp(:,:,:,1) + 2 * pi) .* mask;
Pexp(:,:,:,3) = (Pexp(:,:,:,3) + 0 * pi) .* mask;
Pexp(:,:,:,6) = (Pexp(:,:,:,6) + 0 * pi) .* mask;

%Scale and intercept
Pexp = Pexp/ (4*pi) + 0.5;

EX  = [Mexp(ax1,ax2,ax3,1),Pexp(ax1,ax2,ax3,1),hsp,...
       Mexp(ax1,ax2,ax3,3),Pexp(ax1,ax2,ax3,3),hsp,...
       Mexp(ax1,ax2,ax3,6),Pexp(ax1,ax2,ax3,6);];

%% HS phantom

%Magnitude and phase
M1P1 = abs(p1.STp1);
P1P1 = p1.STp1_uw;
M1P2 = abs(p1.STp2);
P1P2 = p1.STp2_uw;

%Shape
ax1 = 46:150; ax2 = 21:175; ax3 = 40;

%Offset correction
P1P1(:,:,:,1) = (P1P1(:,:,:,1) - 1 * pi) .* mask;
P1P1(:,:,:,3) = (P1P1(:,:,:,3) - 1 * pi) .* mask;
P1P1(:,:,:,6) = (P1P1(:,:,:,6) - 2 * pi) .* mask;

P1P2(:,:,:,1) = (P1P2(:,:,:,1) - 0 * pi) .* mask;
P1P2(:,:,:,3) = (P1P2(:,:,:,3) - 2 * pi) .* mask;
P1P2(:,:,:,5) = (P1P2(:,:,:,5) - 0 * pi) .* mask;


%Scale and intercept
P1P1 = P1P1 / (4*pi) + 0.5;
P1P2 = P1P2 / (4*pi) + 0.5;

HS1   = [M1P1(ax1,ax2,ax3,1),P1P1(ax1,ax2,ax3,1),hsp,...
        M1P1(ax1,ax2,ax3,3),P1P1(ax1,ax2,ax3,3),hsp,...
        M1P1(ax1,ax2,ax3,6),P1P1(ax1,ax2,ax3,6)];
HS2   = [M1P2(ax1,ax2,ax3,1),P1P2(ax1,ax2,ax3,1),hsp,...
        M1P2(ax1,ax2,ax3,3),P1P2(ax1,ax2,ax3,3),hsp,...
        M1P2(ax1,ax2,ax3,5),P1P2(ax1,ax2,ax3,5)];

%% PL phantom

%Magnitude and phase
M2P1 = abs(p2.STp1);
P2P1 = p2.STp1_uw;
M2P2 = abs(p2.STp2);
P2P2 = p2.STp2_uw;

%Shape
ax1 = 46:150; ax2 = 21:175; ax3 = 40;

%Offset correction
P2P1(:,:,:,1) = (P2P1(:,:,:,1) + 1 * pi) .* mask;
P2P1(:,:,:,3) = (P2P1(:,:,:,3) - 1 * pi) .* mask;
P2P1(:,:,:,6) = (P2P1(:,:,:,6) - 2 * pi) .* mask;

P2P2(:,:,:,1) = (P2P2(:,:,:,1) + 2 * pi) .* mask;
P2P2(:,:,:,3) = (P2P2(:,:,:,3) - 2 * pi) .* mask;
P2P2(:,:,:,5) = (P2P2(:,:,:,5) - 0 * pi) .* mask;

%Scale and intercept
P2P1 = P2P1 / (4*pi) + 0.5;
P2P2 = P2P2 / (4*pi) + 0.5;

PL1   = [M2P1(ax1,ax2,ax3,1),P2P1(ax1,ax2,ax3,1),hsp,...
        M2P1(ax1,ax2,ax3,3),P2P1(ax1,ax2,ax3,3),hsp,...
        M2P1(ax1,ax2,ax3,6),P2P1(ax1,ax2,ax3,6)];
PL2   = [M2P2(ax1,ax2,ax3,1),P2P2(ax1,ax2,ax3,1),hsp,...
        M2P2(ax1,ax2,ax3,3),P2P2(ax1,ax2,ax3,3),hsp,...
        M2P2(ax1,ax2,ax3,5),P2P2(ax1,ax2,ax3,5)];

%% IO phantom

%Magnitude and phase
M3P1 = abs(p3.STp1);
P3P1 = p3.STp1_uw;
M3P2 = abs(p3.STp2);
P3P2 = p3.STp2_uw;

%Shape
ax1 = 46:150; ax2 = 21:175; ax3 = 40;

P3P1(:,:,:,1) = (P3P1(:,:,:,1) + 1 * pi) .* mask;
P3P1(:,:,:,3) = (P3P1(:,:,:,3) - 1 * pi) .* mask;
P3P1(:,:,:,6) = (P3P1(:,:,:,6) - 2 * pi) .* mask;

P3P2(:,:,:,1) = (P3P2(:,:,:,1) - 0 * pi) .* mask;
P3P2(:,:,:,3) = (P3P2(:,:,:,3) - 2 * pi) .* mask;
P3P2(:,:,:,5) = (P3P2(:,:,:,5) - 0 * pi) .* mask;

%Scale and intercept
P3P1 = P3P1 / (4*pi) + 0.5;
P3P2 = P3P2 / (4*pi) + 0.5;

IO1  = [M3P1(ax1,ax2,ax3,1),P3P1(ax1,ax2,ax3,1),hsp,...
        M3P1(ax1,ax2,ax3,3),P3P1(ax1,ax2,ax3,3),hsp,...
        M3P1(ax1,ax2,ax3,6),P3P1(ax1,ax2,ax3,6)];
IO2  = [M3P2(ax1,ax2,ax3,1),P3P2(ax1,ax2,ax3,1),hsp,...
        M3P2(ax1,ax2,ax3,3),P3P2(ax1,ax2,ax3,3),hsp,...
        M3P2(ax1,ax2,ax3,5),P3P2(ax1,ax2,ax3,5)];

%% Display    
figure;imshow([EX;vsp;HS1;PL1;IO1;vsp;HS2;PL2;IO2],[0,1]);
set(gca,'Position',[0 0 1 1]);
print('Figure_3','-dpng','-r300')
