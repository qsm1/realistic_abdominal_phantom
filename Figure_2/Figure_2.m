%% Load Variables and phantoms
load ../tissue_properties/exp_properties.mat
p1 = load('Fig2_HS.mat');
p2 = load('Fig2_PL.mat');
p3 = load('Fig2_IO.mat');
mask = p1.mask;

%% Susceptibility Maps

hsp = 1000*ones([105,10]);

%Axial Views
axchi_ex = padarray(qsm_zeroref(:,:,6:85).*mask,[0,0,56],0,'both');   
axchi_p1 = padarray(p1.chi_gt .* mask,[0,0,56],0,'both');
axchi_p2 = padarray(p2.chi_gt .* mask,[0,0,56],0,'both');
axchi_p3 = padarray(p3.chi_gt .* mask,[0,0,56],0,'both');

%Sagittal Views
sgchi_ex = flip(permute(axchi_ex,[3,1,2]),1);
sgchi_p1 = flip(permute(axchi_p1,[3,1,2]),1);
sgchi_p2 = flip(permute(axchi_p2,[3,1,2]),1);
sgchi_p3 = flip(permute(axchi_p3,[3,1,2]),1);

%Shape
ax1 = 46:150; ax2 = 21:175; ax3 = 96;
sg1 = 46:150; sg2 = 50:160; sg3 = 125;

%Concatenate images
chi_fig = [axchi_ex(ax1,ax2,ax3),sgchi_ex(sg1,sg2,sg3),hsp,...
               axchi_p1(ax1,ax2,ax3),sgchi_p1(sg1,sg2,sg3),hsp,...
               axchi_p2(ax1,ax2,ax3),sgchi_p2(sg1,sg2,sg3),hsp,...
               axchi_p3(ax1,ax2,ax3),sgchi_p3(sg1,sg2,sg3)];

%Scale and intercept
chi_fig = chi_fig * 100 + 100;


%% Total Field

%Axial Views
offset = 3*pi/4; %phase offset for fair comparison
axphi_ex = padarray((expphi/42.58/3 - offset).*mask,[0,0,56],0,'both');     
axphi_p1 = padarray(p1.phi.*mask,[0,0,56],0,'both');
axphi_p2 = padarray(p2.phi.*mask,[0,0,56],0,'both');
axphi_p3 = padarray(p3.phi.*mask,[0,0,56],0,'both');

%Sagittal Views
sgphi_ex = flip(permute(axphi_ex,[3,1,2]),1);
sgphi_p1 = flip(permute(axphi_p1,[3,1,2]),1);
sgphi_p2 = flip(permute(axphi_p2,[3,1,2]),1);
sgphi_p3 = flip(permute(axphi_p3,[3,1,2]),1);

%Shape
ax1 = 46:150; ax2 = 21:175; ax3 = 96;
sg1 = 46:150; sg2 = 50:160; sg3 = 125;

%Concatenate images
phi_fig = [axphi_ex(ax1,ax2,ax3),sgphi_ex(sg1,sg2,sg3),hsp,...
               axphi_p1(ax1,ax2,ax3),sgphi_p1(sg1,sg2,sg3),hsp,...
               axphi_p2(ax1,ax2,ax3),sgphi_p2(sg1,sg2,sg3),hsp,...
               axphi_p3(ax1,ax2,ax3),sgphi_p3(sg1,sg2,sg3)];

%Scale and intercept
phi_fig = phi_fig * 50 + 200;


%% Local Field

%Axial Views
axphiL_ex = padarray(phiLex .* mask,[0,0,56],0,'both');   
axphiL_p1 = padarray(p1.phiL .* mask,[0,0,56],0,'both');
axphiL_p2 = padarray(p2.phiL .* mask,[0,0,56],0,'both');
axphiL_p3 = padarray(p3.phiL .* mask,[0,0,56],0,'both');

%Sagittal Views
sgphiL_ex = flip(permute(axphiL_ex,[3,1,2]),1);
sgphiL_p1 = flip(permute(axphiL_p1,[3,1,2]),1);
sgphiL_p2 = flip(permute(axphiL_p2,[3,1,2]),1);
sgphiL_p3 = flip(permute(axphiL_p3,[3,1,2]),1);

%%Shape
ax1 = 46:150; ax2 = 21:175; ax3 = 96;
sg1 = 46:150; sg2 = 50:160; sg3 = 125;

%Concatenate images
phiL_fig = [axphiL_ex(ax1,ax2,ax3),sgphiL_ex(sg1,sg2,sg3),hsp,...
               axphiL_p1(ax1,ax2,ax3),sgphiL_p1(sg1,sg2,sg3),hsp,...
               axphiL_p2(ax1,ax2,ax3),sgphiL_p2(sg1,sg2,sg3),hsp,...
               axphiL_p3(ax1,ax2,ax3),sgphiL_p3(sg1,sg2,sg3)];
           
%Scale and intercept
phiL_fig = phiL_fig * 200 + 100;


%% R2star Map

%Axial_views
axr2_p1 = padarray(p1.r2s_synth .* mask,[0,0,56],0,'both');
axr2_p2 = padarray(p2.r2s_synth .* mask,[0,0,56],0,'both');
axr2_p3 = padarray(p3.r2s_synth .* mask,[0,0,56],0,'both');

%Sagittal Views
sgr2_p1 = flip(permute(axr2_p1,[3,1,2]),1);
sgr2_p2 = flip(permute(axr2_p2,[3,1,2]),1);
sgr2_p3 = flip(permute(axr2_p3,[3,1,2]),1);


%Shape
ax1 = 46:150; ax2 = 21:175; ax3 = 96;
sg1 = 46:150; sg2 = 50:160; sg3 = 50;

%Concatenate images
r2s_fig = [axr2_p1(ax1,ax2,ax3),sgr2_p1(sg1,sg2,sg3),hsp,...
               axr2_p1(ax1,ax2,ax3),sgr2_p1(sg1,sg2,sg3),hsp,...
               axr2_p2(ax1,ax2,ax3),sgr2_p2(sg1,sg2,sg3),hsp,...
               axr2_p3(ax1,ax2,ax3),sgr2_p3(sg1,sg2,sg3)];

%Scale and intercept
r2s_fig = r2s_fig * 1 + 0;          



%% Display Image
figure
imshow([chi_fig;  phiL_fig; phi_fig; r2s_fig],[0,200])
set(gca,'Position',[0 0 1 1]);
print('Figure_2','-dpng','-r300')