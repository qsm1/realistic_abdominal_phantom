function [] = QSM_tool(fname)
load([fname,'.mat'])

%% STAR QSM
STAR_phi  = phi_recon .* mask;
STAR_mask = mask;
STAR_B0   = 3;
STAR_H    = [0,0,1]; 
STAR_padsize = [4,4,60];
STAR_voxelsize = voxel_size;

STAR_chi = QSM_star(STAR_phi, STAR_mask,...
                       'B0',STAR_B0,...
                       'H' ,STAR_H,...
                       'padsize',STAR_padsize,...
                       'voxelsize',STAR_voxelsize);
       

STAR_chi  = fieldmap_demean(STAR_chi, mask) .* mask;
STAR_rmse = compute_rmse(STAR_chi .* mask, chi_gt .* mask)
STAR_xsim = compute_xsim(STAR_chi .* mask, chi_gt .* mask)

%% NDI

Vsize     = voxel_size;
Msize     = matrix_size;
pad_size = [4,4,60];

magn = abs(S);
w = zeros(Msize);
den = zeros(Msize);
for i = 1:length(TE)
    w = w + magn(:,:,:,i) .^2 .* TE(i);
    den = den + magn(:,:,:,i) .* TE(i);
end
w = w ./ (den + eps);
w = w ./ max(w(:));
w = w .* mask;

NDI_phi  = phi_recon;
NDI_phi  = padarray(NDI_phi, pad_size, 0, 'both');
NDI_K    = dipole_kernel_fansi(size(NDI_phi),Vsize,0);
NDI_W    = padarray(w, pad_size, 0, 'both');
NDI_mask = padarray(mask, pad_size, 0, 'both');

ndi_params.alpha           = 10^-7.5; 
ndi_params.tau             = 1;
ndi_params.maxOuterIter    = 1000;
ndi_params.isPrecond       = true;
ndi_params.isGPU           = true;
ndi_params.isShowIters     = false;
ndi_params.input           = NDI_phi;
ndi_params.K               = NDI_K;
ndi_params.weight          = NDI_W;
ndi_params.mask            = NDI_mask;


alphas = linspace(-3, -9, 10);
error1 = zeros(size(alphas));
error2 = zeros(size(alphas));

for i = 1:length(alphas)
    ndi_params.alpha  = 10^alphas(i);
    NDI_qsm  = ndi_auto(ndi_params);
    NDI_chi  = NDI_qsm.x(5:end-4,5:end-4,61:end-60);
    NDI_chi  = fieldmap_demean(NDI_chi, mask) .* mask;
    error1(i)   = compute_rmse(NDI_chi .* mask, chi_gt .* mask);
    error2(i)   = compute_xsim(NDI_chi .* mask, chi_gt .* mask);
    pause(3)
end

[~, ii] = min(error1);
ndi_params.alpha  = 10^alphas(ii);
NDI_qsm  = ndi_auto(ndi_params);
NDI_chi  = NDI_qsm.x(5:end-4,5:end-4,61:end-60);
NDI_chi  = fieldmap_demean(NDI_chi, mask) .* mask;
NDI_rmse = compute_rmse(NDI_chi .* mask, chi_gt .* mask);
NDI_xsim = compute_xsim(NDI_chi .* mask, chi_gt .* mask);


%% MEDI

Vsize     = voxel_size;
Msize     = matrix_size;

magn = abs(S);
w = zeros(Msize);
den = zeros(Msize);
for i = 1:length(TE)
    w = w + magn(:,:,:,i) .^2 .* TE(i);
    den = den + magn(:,:,:,i) .* TE(i);
end
w = w ./ (den + eps);
w = w ./ max(w(:));
w = w .* mask;

pad_size = [4,4,60];
MEDI_phi  = phi_recon;
MEDI_phi  = padarray(MEDI_phi, pad_size, 0, 'both');
MEDI_W    = padarray(w, pad_size, 0, 'both');
MEDI_mask = padarray(mask, pad_size, 0, 'both');
MEDI_lambda = 800;
delta_TE    = TE(2) - TE(1);
CF          = 42.58 * 3 * 1e6;
B0_dir      = [0,0,1];
matrix_size = size(MEDI_phi);
Mask        = MEDI_mask;
RDF         = MEDI_phi * 2 * pi * CF * 1e-6 * delta_TE;
iFreq       = MEDI_phi * 2 * pi * CF * 1e-6 * delta_TE;
iMag        = MEDI_W;
[~,N_std]   = Fit_ppm_complex(squeeze(S));
N_std       = padarray(N_std, pad_size, 0, 'both');
save RDF.mat RDF iFreq iMag N_std Mask matrix_size voxel_size delta_TE CF B0_dir;
matrix_size  = Msize;

MEDI_chi  = MEDI_L1('lambda',MEDI_lambda,'merit');
MEDI_chi  = MEDI_chi(5:end-4,5:end-4,61:end-60);
MEDI_chi  = fieldmap_demean(MEDI_chi, mask) .* mask;
MEDI_rmse = compute_rmse(MEDI_chi .* mask, chi_gt .* mask);
MEDI_xsim = compute_xsim(MEDI_chi .* mask, chi_gt .* mask);

delete RDF.mat
delete results/x00000001.mat


%% FANSI QSM

Vsize     = voxel_size;
Msize     = matrix_size;

magn = abs(S);
w = zeros(Msize);
den = zeros(Msize);
for i = 1:length(TE)
    w = w + magn(:,:,:,i) .^2 .* TE(i);
    den = den + magn(:,:,:,i) .* TE(i);
end
w = w ./ (den + eps);
w = w ./ max(w(:));
w = w .* mask;
pad_size = [4,4,60];

FANSI_phi = phi_recon;
FANSI_phi = padarray(FANSI_phi, pad_size, 0, 'both');
FANSI_K   = dipole_kernel_fansi(size(FANSI_phi),Vsize,0);
FANSI_W   = padarray(w, pad_size, 0, 'both');
FANSI_mask = padarray(mask, pad_size, 0, 'both');

fansi_params = [];
fansi_params.input        = FANSI_phi;
fansi_params.K            = FANSI_K;
fansi_params.mu2          = 1; 
fansi_params.weight       = FANSI_W;
fansi_params.isGPU        = true;
fansi_params.maxOuterIter = 1000;
fansi_params.tolUpdate    = 1e-3;
fansi_params.mask         = FANSI_mask;

alphas = linspace(-3.5, -4.5, 10);
error1 = zeros(size(alphas));
error2 = zeros(size(alphas));

for i = 1:length(alphas)
    fansi_params.alpha1       = 10^alphas(i);
    FANSI_qsm_p1        = nlTV(fansi_params);
    FANSI_chi_p1  = FANSI_qsm_p1.x(5:end-4,5:end-4,61:end-60);
    FANSI_chi_p1  = fieldmap_demean(FANSI_chi_p1, mask) .* mask;
    error1(i)   = compute_rmse(FANSI_chi_p1 .* mask, chi_gt .* mask);
    error2(i)   = compute_xsim(FANSI_chi_p1 .* mask, chi_gt .* mask);
end

[~, ii] = min(error1);
fansi_params.alpha  = 10^alphas(ii);
FANSI_qsm  = nlTV(fansi_params);
FANSI_chi  = FANSI_qsm.x(5:end-4,5:end-4,61:end-60);
FANSI_chi  = fieldmap_demean(FANSI_chi, mask) .* mask;
FANSI_rmse = compute_rmse(FANSI_chi .* mask, chi_gt .* mask);
FANSI_xsim = compute_xsim(FANSI_chi .* mask, chi_gt .* mask);


%% HD-QSM

Vsize     = voxel_size;
Msize     = matrix_size;

magn = abs(S);
w = zeros(Msize);
den = zeros(Msize);
for i = 1:length(TE)
    w = w + magn(:,:,:,i) .^2 .* TE(i);
    den = den + magn(:,:,:,i) .* TE(i);
end
w = w ./ (den + eps);
w = w ./ max(w(:));
w = w .* mask;
pad_size = [4,4,60];

HD_phi = phi_recon;
HD_phi = padarray(HD_phi, pad_size, 0, 'both');
HD_K   = dipole_kernel_fansi(size(HD_phi),Vsize,0);
HD_W   = padarray(w, pad_size, 0, 'both');
HD_mask = padarray(mask, pad_size, 0, 'both');

hd_params = [];
hd_params.input        = HD_phi;
hd_params.kernel       = HD_K;
hd_params.weight       = HD_W;
hd_params.isGPU        = true;
hd_params.maxOuterIter = 1000;
hd_params.tolUpdate    = 1e-3;
hd_params.mask         = HD_mask;

alphas = linspace(-3.5, -4.5, 10);
error1 = zeros(size(alphas));
error2 = zeros(size(alphas));

for i = 1:length(alphas)
    hd_params.alphaL2       = 10^alphas(i);
    hd_params.mu1L2 = 10 * hdparams.alphaL2;
    HD_qsm     = HDQSM(hd_params);
    HD_chi_p1  = HD_qsm.x(5:end-4,5:end-4,61:end-60);
    HD_chi_p1  = fieldmap_demean(HD_chi_p1, mask) .* mask;
    error1(i)  = compute_rmse(HD_chi_p1 .* mask, chi_gt .* mask);
    error2(i)  = compute_xsim(HD_chi_p1 .* mask, chi_gt .* mask);
end

[~, ii] = min(error1);
hd_params.alphaL2  = 10^alphas(ii);
hd_params.mu1L2    = 10 * hdparams.alphaL2;
HD_qsm  = HDQSM(hd_params);
HD_chi  = HD_qsm.x(5:end-4,5:end-4,61:end-60);
HD_chi  = fieldmap_demean(HD_chi, mask) .* mask;
HD_rmse = compute_rmse(HD_chi .* mask, chi_gt .* mask);
HD_xsim = compute_xsim(HD_chi .* mask, chi_gt .* mask);


%%
save(['Figure_5/',fname, '_QSM.mat'], 'STAR_chi','NDI_chi','MEDI_chi','FANSI_chi','HD_chi',...
                          'STAR_rmse','NDI_rmse','MEDI_rmse','FANSI_rmse','HD_rmse',...
                          'STAR_xsim','NDI_xsim','MEDI_xsim','FANSI_xsim','HD_xsim','chi_gt','mask');
end