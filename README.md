## Towards a realistic in-silico abdominal QSM phantom

### Getting started
Please download and install the Following toolboxes.

- FANSI Toolbox: https://gitlab.com/cmilovic/FANSI-toolbox
- HD-QSM: https://github.com/mglambert/HD-QSM
- STI Suite: https://people.eecs.berkeley.edu/~chunlei.liu/software.html
- MEDI Toolbox: http://pre.weill.cornell.edu/mri/pages/qsm.html
- Water-Fat Separation Toolbox: https://www.ismrm.org/workshops/FatWater12/data.htm
- ROMEO: https://github.com/korbinian90/ROMEO


### Citing
If you have used realistic_abdominal_qsm_phantom in a scientific publication, we would appreciate citations to the following works:
- Silva J, Milovic C,Lambert M, et al. Toward a realistic in silico abdominal phantom for QSM. Magn Reson Med. 2023;1-17. doi: 10.1002/mrm.29597
- Milovic C, Bilgic B, Zhao B, Acosta-Cabronero J, Tejos C. Fast nonlinear susceptibility inversion with variational regularization. Magn Reson Med. 2018;80(2):814-821. doi:10.1002/mrm.27073

### Acknowledgment
This work was funded by Fondecyt 1181057, Fondecyt 1191710, Fondecyt 1231535, Anid/PIA/Anillo ACT192064, and Millennium Institute for Intelligent Healthcare Engineering CN2021_004. 
Dr Carlos Milovic was supported by Cancer Research UK Multidisciplinary Award C53545/A24348. 
Dr Cristobal Arrieta was partially funded by ANID Fondecyt Postdoctorado 2019 #3190763.

### Bugs ad Questions
Please contact Javier Silva Orellana at jisilva8@uc.cl

### Disclaimer
THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL JAVIER SILVA OR HIS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE, DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

