%% Variables and model

% Load Variables
fpath = 'tissue_properties/';
load([fpath, 'labels']);
load([fpath, 'r2s']);
load([fpath, 'ph0']);
load([fpath, 'wtr']);
load([fpath, 'fat']);
load([fpath, 'r2s_norm']);
load([fpath, 'wtr_norm']);
load([fpath, 'fat_norm']);

% Load Model
fpath = 'models/paper/HS/';
chi_model = csvread([fpath,'chi_model.csv'],1,1);
r2s_model = csvread([fpath,'r2s_model.csv'],1,1);
ph0 = zeros(size(fat_norm));

% Parameters
voxel_size = [1.9792, 1.9792, 2.0000];
fatmodel.fat_ramps  = [ 0.087,  0.693,  0.128,  0.004,  0.039, 0.048];
fatmodel.fat_freqs  = [-3.800, -3.400, -2.600, -1.940, -0.390, 0.600];

% Fieldmap

%Local susceptibility
[chi_L, mask] = fieldmap_chi_assign(chi_model,labels,wtr_norm,fat_norm,r2s_norm);
[chi_L, mask] = fieldmap_exclude_tissues(chi_L,mask,labels,[12,21,23]);
[chi_L] = partial_volumes(chi_L, labels, 0.5, [12,21,23]);
[chi_gt] = fieldmap_demean(chi_L, mask);

%Background susceptibility
chi_BG = fieldmap_background(chi_model,labels,[12,23],9.2);

%Dipole convolution
chi_pad = fieldmap_padding(chi_L, 0);
phiL = fieldmap_dipole_convolution(chi_pad,voxel_size,0);
chi_pad = fieldmap_padding(chi_L + chi_BG, 1);
phi = fieldmap_dipole_convolution(chi_pad,voxel_size,0);

%R2s map
[r2s_synth,r2s_mask] = r2star_r2s_assign(r2s_model,labels,r2s_norm,r2s);
[r2s_synth] = partial_volumes(r2s_synth, r2s_mask, 0.5, []);

%% Signal model

%Parameters
TEp1 = linspace(1.8, 9.6, 6);
TEp2 = linspace(2.3, 11.5, 5); 
SNR   = 100;

% Signal
[pdff_gt, STp1, SufTp1, ~, ~] = signalmodel_simulate(phi, r2s_synth, mask, wtr, fat, ph0, TEp1, fatmodel);
[~, SLp1, SufLp1, ~, ~] = signalmodel_simulate(phiL, r2s_synth, mask, wtr, fat, ph0, TEp1, fatmodel);

[~, STp2, SufTp2, ~, ~] = signalmodel_simulate(phi, r2s_synth, mask, wtr, fat, ph0, TEp2, fatmodel);
[~, SLp2, SufLp2, ~, ~] = signalmodel_simulate(phiL, r2s_synth, mask, wtr, fat, ph0, TEp2, fatmodel);

% Noise
noisep1 = (1/SNR) * (randn(size(STp1)) + 1i * randn(size(STp1)));
noisep2 = (1/SNR) * (randn(size(STp2)) + 1i * randn(size(STp2))); 

STp1   = STp1 + noisep1;
STp2   = STp2 + noisep2;
SLp1   = SLp1 + noisep1;
SLp2   = SLp2 + noisep2;
SufTp1   = SufTp1 + noisep1;
SufTp2   = SufTp2 + noisep2;
SufLp1   = SufLp1 + noisep1;
SufLp2   = SufLp2 + noisep2;

%% Fieldmap Estimation
% Graph Cuts
lambda = 0.05; iters = 40;

[phiT_gc, r2sT_gc, ~, ~, ~] = FieldmapEstimation_GC(STp1, TEp1, fatmodel, lambda, iters);
[phiL_gc, r2sL_gc, ~, ~, ~] = FieldmapEstimation_GC(SLp1, TEp1, fatmodel, lambda, iters);


%% ROMEO
%Phase unwrapping
[phiT_ip_uw] = phaseunwrapping_ROMEO(STp2, TEp2, mask) .* mask;
[phiL_ip_uw] = phaseunwrapping_ROMEO(SLp2, TEp2, mask) .* mask;

%Echo combination
[~, phiT_ip,~] = linearLSQR( phiT_ip_uw, TEp2/1000, abs(SLp2));
[~, phiL_ip,~] = linearLSQR( phiL_ip_uw, TEp2/1000, abs(SLp2));

%R2* map
[~, r2sT_ip, ~] = t2s( abs(STp2), TEp2/1000 , ones(size(STp2)));
[~, r2sL_ip, ~] = t2s( abs(SLp2), TEp2/1000 , ones(size(SLp2)));

%Phase scale
phase_scale = 2 * pi * 3 * 42.58;
phiT_ip = phiT_ip / phase_scale;
phiL_ip = phiL_ip / phase_scale;

%% Discrepancy
[SufTp2_ip] = phaseunwrapping_ROMEO(SufTp2, TEp2, mask) .* mask;
[SufLp2_ip] = phaseunwrapping_ROMEO(SufLp2, TEp2, mask) .* mask;

%% Save
save('Figure_4/Fig4_HS.mat','phiT_gc', 'r2sT_gc', 'phiL_gc', 'r2sL_gc', ...
    'phiT_ip_uw', 'phiL_ip_uw', 'phiT_ip', 'phiL_ip', 'SufTp2_ip', 'SufLp2_ip',...
    'r2sT_ip', 'r2sL_ip');