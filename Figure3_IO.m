%% Variables and model

% Load Variables
fpath = 'tissue_properties/';
load([fpath, 'labels']);
load([fpath, 'r2s']);
load([fpath, 'ph0']);
load([fpath, 'wtr']);
load([fpath, 'fat']);
load([fpath, 'r2s_norm']);
load([fpath, 'wtr_norm']);
load([fpath, 'fat_norm']);

% Load Model
fpath = 'models/paper/IO/';
chi_model = csvread([fpath,'chi_model.csv'],1,1);
r2s_model = csvread([fpath,'r2s_model.csv'],1,1);
ph0 = zeros(size(fat_norm));

% Parameters
voxel_size = [1.9792, 1.9792, 2.0000];
fatmodel.fat_ramps  = [ 0.087,  0.693,  0.128,  0.004,  0.039, 0.048];
fatmodel.fat_freqs  = [-3.800, -3.400, -2.600, -1.940, -0.390, 0.600];

% Fieldmap

%Local susceptibility
[chi_L, mask] = fieldmap_chi_assign(chi_model,labels,wtr_norm,fat_norm,r2s_norm);
[chi_L, mask] = fieldmap_exclude_tissues(chi_L,mask,labels,[12,21,23]);
[chi_L] = partial_volumes(chi_L, labels, 0.5, [12,21,23]);
[chi_gt] = fieldmap_demean(chi_L, mask);

%Background susceptibility
chi_BG = fieldmap_background(chi_model,labels,[12,23],9.2);

%Dipole convolution
chi_pad = fieldmap_padding(chi_L, 0);
phiL = fieldmap_dipole_convolution(chi_pad,voxel_size,0);
chi_pad = fieldmap_padding(chi_L + chi_BG, 1);
phi = fieldmap_dipole_convolution(chi_pad,voxel_size,0);

%R2s map
[r2s_synth,r2s_mask] = r2star_r2s_assign(r2s_model,labels,r2s_norm,r2s);
[r2s_synth] = partial_volumes(r2s_synth, r2s_mask, 0.5, []);

%% Signal model

%Parameters
TEp1 = linspace(1.8, 9.6, 6);
TEp2 = linspace(2.3, 11.5, 5); 
SNR   = 100;

% Signal
[~, STp1, ~, ~, ~] = signalmodel_simulate(phi, r2s_synth, mask, wtr, fat, ph0, TEp1, fatmodel);
[~, STp2, ~, ~, ~] = signalmodel_simulate(phi, r2s_synth, mask, wtr, fat, ph0, TEp2, fatmodel);

% Noise
noisep1 = (1/SNR) * (randn(size(STp1)) + 1i * randn(size(STp1)));
noisep2 = (1/SNR) * (randn(size(STp2)) + 1i * randn(size(STp2))); 
STp1   = STp1 + noisep1;
STp2   = STp2 + noisep2;

%Unwrap
STp1_uw = zeros(size(STp1));
STp2_uw = zeros(size(STp1));

for i = 1:length(TEp1)
    STp1_uw(:,:,:,i) = FieldmapEstimation_ROMEO(STp1(:,:,:,i),TEp1(i),mask);
end

for i = 1:length(TEp2)
    STp2_uw(:,:,:,i) = FieldmapEstimation_ROMEO(STp2(:,:,:,i),TEp2(i),mask);
end

%save
save('Figure_3/Fig3_IO.mat','STp1','STp2','STp1_uw','STp2_uw')